<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Controller\TokenController;


class LandingController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function start(){
        return $this->render('api/index.html.twig', [
            'heading' => 'Welcome',
            'title' => 'Json Web Token Auth'
        ]);
    }

    /**
     * @Route("/api", name="api")
     */
    public function index()
    {
    	header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Authorization");
		header("Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE");

        // return $this->render('api/index.html.twig', [
        //     'heading' => 'Welcome',
        //     'title' => 'Json Web Token Auth'
        // ]);
        $response = new JsonResponse(array(
            'msg' => 'Hello from this endpoint.'
        ));

        return $response;
    }

    /**
     * @Route("/api/public", name="public")
     */
    public function publicApi(){
    	//$app = new TokenController();
    	header('Content-Type: application/json; charset=utf-8');
		//echo json_encode($app->publicEndpoint());

    	// return $this->render('api/public.html.twig', [
     //        'heading' => 'Welcome',
     //        'title' => 'Json Web Token Auth'
     //    ]);
        $response = new JsonResponse(array(
            'msg' => 'Hello from the public endpoint.'
        ));

        return $response;
    }

    /**
     * @Route("/api/private", name="private")
     */
    public function privateAction(){
    	$app = new TokenController();
    	header('Content-Type: application/json; charset=utf-8');
		// echo json_encode($app->privateEndpoint());

    	$requestHeaders = apache_request_headers();

		if (!isset($requestHeaders['authorization']) && 
			!isset($requestHeaders['Authorization'])){
			header('HTTP/1.0 401 Unauthorized');
			header('Content-Type: application/json; charset=utf-8');

			$response = new JsonResponse(array(
                'msg' => 'Hello, you did not provide any tokens.'
            ));

            return $response;
		}

		$authorizationHeader = isset($requestHeaders['authorization']) ? $requestHeaders['authorization'] : $requestHeaders['Authorization'];

		if ($authorizationHeader == null) {
			header('HTTP/1.0 401 Unauthorized');
			header('Content-Type: application/json; charset=utf-8');

			$response = new JsonResponse(array(
                'msg' => 'Access to this endpoint is restricted.'
            ));

            return $response;
		}

		$authorizationHeader = str_replace('bearer ', '', $authorizationHeader);

		try {
			$app->setCurrentToken($authorizationHeader);
		} catch (\Auth0\SDK\Exception\CoreException $e) {
			header('HTTP/1.0 401 Unauthorized');
			header('Content-Type: application/json; charset=utf-8');

			$response = new JsonResponse(array(
                'msg' => $e->getMessage()
            ));

            return $response;
		}

    	$response = new JsonResponse(array(
            'msg' => 'Hello from private endpoint.'
        ));

        return $response;
    }

    /**
     * @Route("/api/private/scoped", name="private_scoped")
     */
    public function scoped(){
        $app = new TokenController();
    	header('Content-Type" application/json; charset=utf-8');
		$message = json_encode($app->privateScopeEndpoint());
        
        $response = new JsonResponse(array(
            'msg' => $message
        ));

        return $response;
    }
}
