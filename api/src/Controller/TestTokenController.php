<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Dotenv\Dotenv;

use Auth0\SDK\API\Authentication;
use Auth0\SDK\Exception\ApiException;
use GuzzleHttp\Exception\ClientException;

class TestTokenController extends AbstractController
{
    /**
     * @Route("/test/token", name="test_token")
     */
    public function index()
    {
    	$dotenv = new Dotenv();
    	$dotenv->load(__DIR__.'/../../.env');
    	$client_id = (string) getenv('AUTH0_CLIENT_ID');
    	$test_token = $this->generateToken();

        return $this->render('test_token/test.html.twig', [
        	'title' => 'Test',
        	'client_id' => $client_id,
        	'test' => $test_token
        ]);
    }

    /**
     * Generate an access token.
     * @return access token
     */

    public function generateToken(){
    	$auth0_api = new Authentication(getenv('AUTH0_DOMAIN'));

    	$config = [
    		'client_secret' => getenv('AUTH0_CLIENT_SECRET'),
    		'client_id' => getenv('AUTH0_CLIENT_ID'),
    		'audience' => getenv('AUTH0_MANAGEMENT_AUDIENCE'),
    		'grant_type' => getenv('AUTH0_GRANT_TYPE'),
    	];

    	try {
    		$result = $auth0_api->client_credentials($config);
    		echo '<pre>' . print_r($result, true) . '</pre>';
    		die();
    	} catch (ClientException $e){
    		echo "ClientException: " . $e->getMessage();
    	} catch(ApiException $e) {
    		echo "ApiException: " . $e->getMessage();
    	}
    }


}
