<?php

/* api/public.html.twig */
class __TwigTemplate_349ed54c9c8c37413adc5e3fbb5893ec618fab05956b20625133fd0d9a9bbffb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "api/public.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayBlock('content', $context, $blocks);
        // line 19
        echo "
";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "        <!-- Banner -->
\t\t<section id=\"intro\" class=\"main\">
\t\t\t<!-- <span class=\"icon fa-diamond major\"></span> -->
\t\t\t<h2>";
        // line 9
        echo twig_escape_filter($this->env, ($context["heading"] ?? null), "html", null, true);
        echo "</h2>
\t\t\t<p>This is the public landing page. It is available to the general public.</p>
\t\t\t<ul class=\"actions\">
\t\t\t\t<li><a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api");
        echo "\" class=\"button big\">Home</a></li>
\t\t\t\t<li><a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("private");
        echo "\" class=\"button big\">Private</a></li>
\t\t\t</ul>
\t\t</section>

\t\t<!-- Items -->
    ";
    }

    public function getTemplateName()
    {
        return "api/public.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 13,  60 => 12,  54 => 9,  49 => 6,  46 => 5,  41 => 19,  39 => 5,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "api/public.html.twig", "/var/www/crypto/api/templates/api/public.html.twig");
    }
}
