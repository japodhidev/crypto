﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

using Token;
using Rest;

namespace RSACrypto
{
    public class CryptoRSA
    {

        static void Main(){

            JWToken token = new JWToken();
            //token.CreateToken();
            string key = token.CreateToken();
            //token.DecodeToken(key);
            RestClient client = new RestClient();
            Task task = client.RestRequest();
            task.Wait();

            Task postData = client.PostAsync(key);
            postData.Wait();
        }

        public static string Connect(string data)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            WebClient customClient = new WebClient();
            string uri = "http://crypto.com/index.php/api/private";
            byte[] postArray = Encoding.ASCII.GetBytes(data);
            NameValueCollection testArray = new NameValueCollection();
            testArray.Add("username", "foo");
            testArray.Add("password", "foobar");
            //testArray.Add("data", data);

            Console.WriteLine("Uploading to {0} ... ", uri);
            customClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            byte[] responseArray = customClient.UploadValues(uri, "POST", testArray);

            string responseBody = Encoding.ASCII.GetString(responseArray);
            Console.WriteLine("\nResponse received was: {0}", responseBody);
            Console.WriteLine("Type: {0}", responseBody.GetType());
            return responseBody;

        }
    }
}
