<?php

/* base.html.twig */
class __TwigTemplate_116d0b9e34e83d66e790aac779a0d368c890b53ce1c4ee82aa51d178cf86c9cd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
\t\t
        <title>
        \t";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        // line 9
        echo "    \t</title>

        ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "    </head>
    <body>

        ";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 39
        echo "

        ";
        // line 41
        $this->displayBlock('javascripts', $context, $blocks);
        // line 47
        echo "    </body>
</html>
";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/css/main.css"), "html", null, true);
        echo "\" />
        ";
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        // line 18
        echo "
            <div id=\"wrapper\">

            \t";
        // line 21
        $this->displayBlock('content', $context, $blocks);
        // line 37
        echo "            </div>
        ";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "            \t    <!-- Banner -->
\t\t\t\t\t<section id=\"intro\" class=\"main\">
\t\t\t\t\t\t<!-- <span class=\"icon fa-diamond major\"></span> -->
\t\t\t\t\t\t<h2>";
        // line 25
        echo twig_escape_filter($this->env, ($context["heading"] ?? null), "html", null, true);
        echo "</h2>
\t\t\t\t\t\t<p>This is the landing page. It is available to the general public.</p>
\t\t\t\t\t\t<ul class=\"actions\">
\t\t\t\t\t\t\t<li><a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api");
        echo "\" class=\"button big\">Home</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("public");
        echo "\" class=\"button big\">Public</a>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("private");
        echo "\" class=\"button big\">Private</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</section>

\t\t\t\t\t<!-- Items -->
\t\t\t\t\t
            \t";
    }

    // line 41
    public function block_javascripts($context, array $blocks = array())
    {
        // line 42
        echo "        \t<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        \t<script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/js/skel.min.js"), "html", null, true);
        echo "\"></script>
        \t<script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/js/util.js"), "html", null, true);
        echo "\"></script>
        \t<script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/js/main.js"), "html", null, true);
        echo "\"></script>
\t\t";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  144 => 45,  140 => 44,  136 => 43,  131 => 42,  128 => 41,  117 => 30,  113 => 29,  109 => 28,  103 => 25,  98 => 22,  95 => 21,  90 => 37,  88 => 21,  83 => 18,  80 => 17,  73 => 12,  70 => 11,  64 => 8,  58 => 47,  56 => 41,  52 => 39,  50 => 17,  45 => 14,  43 => 11,  39 => 9,  37 => 8,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/var/www/crypto/api/templates/base.html.twig");
    }
}
