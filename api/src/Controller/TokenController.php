<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Auth0\SDK\JWTVerifier;
use Symfony\Component\HttpFoundation\JsonResponse;


class TokenController
{
	protected $token;
	protected $token_info;

	public function setCurrentToken($token){

		try {
			$verifier = new JWTVerifier([
				'supported_algs' => ['RS256'],
        		'valid_audiences' => ['595dd4d64738be066917ca10'],
        		'authorized_iss' => ['https://japodhidev.eu.auth0.com/']
			]);

			$this->token = $token;
			$this->tokenInfo = $verifier->verifyAndDecode($token);
		} catch (\Auth0\SDK\Exception\CoreException $e){
			throw $e;
		}
	}
	
	public function publicEndpoint() {
		$message = array(
			"status" => "ok",
			"message" => "Hello from a public endpoint."
		);
		return json_encode($message);
	}

	public function privateEndpoint() {
		$response = new JsonResponse(array(
			'status' => 'ok',
			'message' => 'Hello from a private endpoint.'
		));
		return $response;
	}

	public function checkScope($scope) {
		if ($this->tokenInfo){
			$scopes = explode(" ", $this->tokenInfo->scope);
			foreach ($scopes as $s) {
				if ($s == $scope) 
					return true;
			}
		}
		return false;
	}

	public function privateScopeEndpoint(){
		$response = new JsonResponse(array(
			'status' => 'ok',
			'message' => 'Hello from a private endpoint.'
		));
		return $response
	}
}
