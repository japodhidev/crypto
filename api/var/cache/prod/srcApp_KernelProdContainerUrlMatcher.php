<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelProdContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = array(
            '/api' => array(array(array('_route' => 'api', '_controller' => 'App\\Controller\\LandingController::index'), null, null, null, false, null)),
            '/api/public' => array(array(array('_route' => 'public', '_controller' => 'App\\Controller\\LandingController::publicApi'), null, null, null, false, null)),
            '/api/private' => array(array(array('_route' => 'private', '_controller' => 'App\\Controller\\LandingController::privateAction'), null, null, null, false, null)),
            '/api/private/scoped' => array(array(array('_route' => 'private_scoped', '_controller' => 'App\\Controller\\LandingController::scoped'), null, null, null, false, null)),
        );
    }
}
