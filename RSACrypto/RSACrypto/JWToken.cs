﻿using System;
using System.Collections.Generic;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using JWT.Builder;


namespace Token
{
    public class JWToken
    {
        public string CreateToken() {
            var payload = new Dictionary<string, object>
            {
                {"username", "you"},
                {"email", "me@you.com"}
            };

            const string secret = "reallysecretkey";

            var token = new JwtBuilder()
                .WithAlgorithm(new HMACSHA512Algorithm())
                .WithSecret(secret)
                .AddClaim("name", "you")
                .Build();

            Console.WriteLine(token);
            return token;
        }

        public void DecodeToken(string token) {

            const string secret = "reallysecretkey";

            try {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                var json = decoder.Decode(token, secret, verify: true);
                Console.WriteLine("Decoded payload: {0}", json);
            }
            catch (TokenExpiredException)
            {
                Console.WriteLine("Token has expired.");
            } catch (SignatureVerificationException)
            {
                Console.WriteLine("Token has invalid signature.");
            }
        }
    }
}
