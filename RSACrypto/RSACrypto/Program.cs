﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

using Application;
using QuickType;

namespace RSACrypto
{
    static class Program
    {
       
        static void MainTest() {
            //string datum = Connect("welcome");

            CryptoSodium.GetKeys();
            try {
                // Convert between byte array and string.
                UnicodeEncoding ByteConverter = new UnicodeEncoding();

                // byte arrays to hold data
                // byte[] dataToEncrypt = ByteConverter.GetBytes(datum);
                //Console.WriteLine(Encoding.Default.GetString(dataToEncrypt));

                byte[] dataToEncrypt = ByteConverter.GetBytes("Hey there.");
                //byte[] dataToEncrypt = Convert.FromBase64String(datum);
                Console.WriteLine(Encoding.Default.GetString(dataToEncrypt));
                byte[] encryptedData;
                byte[] decryptedData;


                // Create a new instance os RSACryptoServiceProvider to generate
                // public and private key data
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048))
                {
                    // TODO ExportKeys.ExportPublicKey(RSA);
                    var publicKey = RSA.ExportParameters(false);
                    var privateKey = RSA.ExportParameters(true);

                    /**
                    string pubKeyString = GetKeyString(publicKey);
                    string privKeyString = GetKeyString(privateKey);
                    **/

                    //Console.WriteLine("Public Key: {0}", pubKeyString);

                    var json = Connect("test");
                    Welcome result = JsonConvert.DeserializeObject<Welcome>(json);

                    string [] keyArray = result.Bob.Split(',');
                    List<string> keyList = new List<string>(keyArray.Length);
                    keyList.AddRange(keyArray);
                    //keyList.Reverse();

                    // Console.WriteLine("test: {0}", keyList);
                    foreach(string key in keyList) {
                        int k = Convert.ToInt32(key);
                        byte binKey = (byte)k;
                        Console.WriteLine("[=]: {0}  \t[->]: {1}", k, binKey);
                    }
                    // var welcome = Welcome.FromJson(jsonString);

                    // Console.WriteLine("welcome: {0}", welcome);

                    /*
                     * Pass the data to encrypt, public key information
                     * RSACyptoServiceProvider.ExportParameters(false),
                     * and a boolean flag specifying no OAEP padding
                     */
                    encryptedData = RSAEncrypt(dataToEncrypt, RSA.ExportParameters(false), false);

                    /*
                     * Pass the data to decyrpt, private key information
                     * RSACyptoServiceProvider.ExportParameters(true),
                     * and a boolean flag specifying no OAEP padding
                     */
                    decryptedData = RSADecrypt(encryptedData, RSA.ExportParameters(true), false);

                    // Display the decrypted plaintext to the console
                    Console.WriteLine("Decrypted data: {0}", ByteConverter.GetString(decryptedData));
                }

            } catch (ArgumentNullException){
                Console.WriteLine("Encryption Failed");
            }
        }

        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {

                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }

        public static byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Decrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                }
                return decryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }

        }

        public static string Connect(string data){

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            WebClient customClient = new WebClient();
            string uri = "http://rsa-auth.com";
            //byte[] postArray = Encoding.ASCII.GetBytes(data);
            NameValueCollection testArray = new NameValueCollection();
            testArray.Add("username", "foo");
            testArray.Add("password", "foobar");
            testArray.Add("data", data);

            Console.WriteLine("Uploading to {0} ... ", uri);
            customClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            byte[] responseArray = customClient.UploadValues(uri, "POST", testArray);

            string responseBody = Encoding.ASCII.GetString(responseArray);
            Console.WriteLine("\nResponse received was: {0}", responseBody);
            Console.WriteLine("Type: {0}", responseBody.GetType());
            return responseBody;

        }

        /**
        public static string GetKeyString(RSAParameters pubk){
            var strWriter = new StringWriter();
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            xmlSerializer.Serialize(strWriter, pubk);

            return strWriter.ToString();
        } */


    }
}
