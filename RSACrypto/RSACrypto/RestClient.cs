﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace Rest
{
    public class RestClient
    {
        public async Task RestRequest()
        {

            using (HttpClient client = new HttpClient())
            {
                try
                {

                    HttpResponseMessage httpResponse = await client.GetAsync("http://127.0.0.1:8000/api/public");
                    httpResponse.EnsureSuccessStatusCode();

                    string responseBody = await httpResponse.Content.ReadAsStringAsync();
                    Console.WriteLine("Response: {0}", responseBody);

                } catch (HttpRequestException e)
                {
                    Console.WriteLine("\nException caught");
                    Console.WriteLine("Message: {0} ", e.Message);
                }
            }
        }

        public async Task PostAsync(string access_token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://127.0.0.1:8000");
                var content = new FormUrlEncodedContent(
                    new[]
                    {
                        new KeyValuePair<string, string>("name", "you")
                    }
                );
                string bearer = "Bearer " + access_token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", bearer);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var result = await client.PostAsync("/api/private", content);
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            }
        }
    }
}
